package com.example.dictatekb.util

import androidx.preference.PreferenceManager
import android.content.SharedPreferences
import android.content.Context

private const val KEY_BASE_URL = "baseUrl"

class SettingsHelper(private val context: Context) {
    private val prefs: SharedPreferences

    init {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun getBaseUrl(): String? {
        return prefs.getString(KEY_BASE_URL, null)
    }
}

