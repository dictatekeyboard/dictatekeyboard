package com.example.dictatekb.util

import android.view.View
import android.view.MotionEvent

abstract class TouchListener: View.OnTouchListener {
    // touch handler - touch to record, release to transcribe
    override fun onTouch(view: View, event: MotionEvent): Boolean {
        val action = event.getActionMasked()

        return when(action) {
            MotionEvent.ACTION_DOWN -> {
                onDown()
            }
            MotionEvent.ACTION_UP -> {
                onUp()
            }
            MotionEvent.ACTION_CANCEL -> {
                onCancel()
            }
            else -> false
        }
    }

    public abstract fun onDown(): Boolean
    public abstract fun onUp(): Boolean
    public abstract fun onCancel(): Boolean
}
