package com.example.dictatekb.util

import android.media.MediaRecorder
import android.media.MediaPlayer
import java.util.Base64
import java.io.File

import com.example.dictatekb.error.NoRecordedDataError

// constants for audio settings
private const val AUDIO_SOURCE = MediaRecorder.AudioSource.CAMCORDER
private const val OUTPUT_FORMAT = MediaRecorder.OutputFormat.MPEG_4
private const val ENCODER = MediaRecorder.AudioEncoder.AAC
private const val BIT_RATE = 128000
private const val SAMPLE_RATE = 44100
public const val FILE_EXTENSION = ".mp4"

class AudioHelper(private val fileName: String) {
    private var isRecording: Boolean
    private var isPlaying: Boolean
    private var recordingComplete: Boolean
    private val recorder: MediaRecorder
    private val player: MediaPlayer
    private val encoder: Base64.Encoder
    private var onComplete: (() -> Unit) = {}

    init {
        isRecording = false
        isPlaying = false
        recordingComplete = false

        recorder = MediaRecorder()
        player = MediaPlayer()
        encoder = Base64.getEncoder()

        player.setOnCompletionListener {
            stopPlaying()

            onComplete()
        }

    }

    private fun prepareAndStart() {
        recorder.apply {
			setAudioSource(AUDIO_SOURCE)
            setOutputFormat(OUTPUT_FORMAT)
            setOutputFile(fileName)
            setAudioEncoder(ENCODER)
            setAudioEncodingBitRate(BIT_RATE)
            setAudioSamplingRate(SAMPLE_RATE)

            prepare()
            start()
        }
    }

    // starts recording - throws IllegalStateException if already recording
    fun startRecording() {
        if (isRecording || isPlaying) {
            throw Error("Cannot start recording - recording already in progress.")
        }

        prepareAndStart()
        isRecording = true
        recordingComplete = false
    }

    // stops recording and resets recorder for next time - throws IllegalStateException if not recording
    fun stopRecording() {
        if (!isRecording) {
            throw Error("Cannot stop recording - recording not in progress.")
        }

        isRecording = false
        recorder.apply {
            try {
                stop()
            } catch (e: RuntimeException) {
                throw NoRecordedDataError()
            } finally {
                reset()
            }
        }

        recordingComplete = true
    }

    fun setOnPlaybackFinishedListener(f: (() -> Unit)) {
        onComplete = f
    }

    fun startPlaying() {
        if (isRecording || isPlaying || !recordingComplete) {
            throw Error("Cannot stop recording - recording not in progress.")
        }

        player.apply {
            setDataSource(fileName)
            prepare()
            start()
        }

        isPlaying = true
    }

    fun stopPlaying() {
        if (!isPlaying) {
            throw Error("Cannot stop playback - not playing")
        }

        player.apply {
            stop()
            reset()
        }

        isPlaying = false
    }

    // stops a recording if in progress and releases audio recorder resources
    fun clear() {
        if (isRecording) {
            stopRecording()
        }

        if (isPlaying) {
            stopPlaying()
        }

        recorder.release()
        player.release()
    }

    fun isRecording(): Boolean {
        return isRecording
    }

    fun isPlaying(): Boolean {
        return isPlaying
    }

    // returns a base64 encoded string of the recordid audio - throws an exception if the recording is not complete
    fun getBase64(): String {
        if (!recordingComplete) {
            throw Error("No audio - recording not complete.")
        }

        val audioBytes = File(fileName).readBytes()
        return encoder.encodeToString(audioBytes)
    }
}
