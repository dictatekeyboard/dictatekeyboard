package com.example.dictatekb.api

import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PATCH
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.Call
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

import com.example.dictatekb.util.InvalidatableLazyImpl

// moshi json processor
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private const val ROUTE_TRANSCRIBE = "transcriber/"
private const val ROUTE_SNIPPET = "snippets/"

typealias TranscribeRequest =
    com.example.dictatekb.api.request.Transcribe
typealias TranscribeResponse =
    com.example.dictatekb.api.response.Transcribe
typealias SnippetRequest =
    com.example.dictatekb.api.request.Snippet
typealias SnippetResponse =
    com.example.dictatekb.api.response.Snippet

// Speech Api interface
interface TranscribeApiService {
    @Headers("accept: application/json")
    @POST(ROUTE_TRANSCRIBE)
    fun transcribe(@Body req: TranscribeRequest): Call<TranscribeResponse>

    @Headers("accept: application/json")
    @GET(ROUTE_SNIPPET)
    fun nextSnippet(): Call<SnippetResponse>

    @Headers("accept: application/json")
    @PATCH(ROUTE_SNIPPET + "{snippet_id}")
    fun submitSnippet(@Path(value = "snippet_id") id: Int, @Body req: SnippetRequest): Call<SnippetResponse>
}

class Api {
    companion object {
        // to validate urls, ensures they start with [http|https]://, and end with a /.
        private val urlRegex = "https?://.*/$".toRegex()

        private var baseUrl: String = ""

        // invalidatable lazy initialiser
        private val serviceDelegate = InvalidatableLazyImpl({
            val retrofit = Retrofit.Builder()
               .addConverterFactory(MoshiConverterFactory.create(moshi))
                .baseUrl(baseUrl)
                .build()

            retrofit.create(
                TranscribeApiService::class.java
            )
        })
        val retrofitService: TranscribeApiService by serviceDelegate

        fun init(newUrl: String?) {
            if (newUrl == null || newUrl == "") {
                throw IllegalArgumentException("Base URL not initialised.")
            }

            if (!urlRegex.matches(newUrl)) {
                throw IllegalArgumentException("Invalid URL, please ensure it begins with http/https, and ends with a /.")
            }

            if (newUrl == baseUrl) {
                return
            }

            // re-assign url, and invalidade service
            baseUrl = newUrl
            serviceDelegate.invalidate()
        }
    }
}

