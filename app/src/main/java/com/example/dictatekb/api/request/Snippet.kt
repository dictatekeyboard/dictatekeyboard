package com.example.dictatekb.api.request

/**
 * Training snippet json body
 * https://api.hameed.info/mesar/speech/docs/transcriber
 */
data class Snippet (
    val id: Int,
    val status: String,
    val audio: String,
    val token: String
)

