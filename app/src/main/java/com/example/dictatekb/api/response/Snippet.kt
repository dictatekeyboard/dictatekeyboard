package com.example.dictatekb.api.response

/**
 * Training snippet json body
 * https://api.hameed.info/mesar/speech/docs/transcriber
 */
data class Snippet (
    val id: Int,
    val snippet: String,
    val status: String,
    val token: String?
)

