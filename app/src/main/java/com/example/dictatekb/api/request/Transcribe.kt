package com.example.dictatekb.api.request

/**
 * Text transcription json request body
 * https://api.hameed.info/mesar/speech/docs/transcriber
 */
data class Transcribe (
    val audio: String
)

