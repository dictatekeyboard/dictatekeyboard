package com.example.dictatekb.api.response

/**
 * Text transcription json response body
 * https://api.hameed.info/mesar/speech/docs/transcriber
 */
data class Transcribe (
    val transcripts: List<Transcript>
)

data class Transcript (
    val confidence: Float,
    val utterance: String
)

