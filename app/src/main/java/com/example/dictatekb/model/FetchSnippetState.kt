package com.example.dictatekb.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Callback
import retrofit2.Call
import retrofit2.Response
import android.util.Log

import com.example.dictatekb.api.Api
import com.example.dictatekb.api.response.Snippet
import com.example.dictatekb.util.SettingsHelper

class FetchSnippetState(helper: SettingsHelper): AbstractApiState<Unit, Snippet>(helper) {
    override fun makeRequest(req: Unit) {
        initApi()
        if (_error.value != null) {
            return
        }

        _isLoading.value = true
        Api.retrofitService.nextSnippet().enqueue(
            object: Callback<Snippet> {
                override fun onFailure(call: Call<Snippet>, t: Throwable) {
                    _isLoading.value = false
                    _error.value = t.toString()
                }

                override fun onResponse(call: Call<Snippet>, response: Response<Snippet>) {
                    _isLoading.value = false
                    if (!response.isSuccessful()) {
                        _error.value = ("Error " + response.code() + " " +
                            (response.message() ?: "Unknown Error"))

                        return
                    }

                    _response.value = response.body()
                }
            }
        )
    }
}
