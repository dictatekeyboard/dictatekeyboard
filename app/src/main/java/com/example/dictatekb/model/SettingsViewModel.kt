package com.example.dictatekb.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData

import com.example.dictatekb.util.SettingsHelper

class SettingsViewModel(private val helper: SettingsHelper) : ViewModel() {
    var baseUrl: MutableLiveData<String>

    init {
        baseUrl = MutableLiveData<String>()

        reloadSettings()
    }

    fun reloadSettings() {
        baseUrl.value = helper.getBaseUrl()
    }
}

