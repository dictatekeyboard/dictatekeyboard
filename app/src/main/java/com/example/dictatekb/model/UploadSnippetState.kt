package com.example.dictatekb.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Callback
import retrofit2.Call
import retrofit2.Response
import android.util.Log
import java.lang.Exception

import com.example.dictatekb.api.Api
import com.example.dictatekb.api.response.Snippet
import com.example.dictatekb.util.SettingsHelper
typealias SnippetRequest =
    com.example.dictatekb.api.request.Snippet

class UploadSnippetState(helper: SettingsHelper): AbstractApiState<SnippetRequest, Snippet>(helper) {
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun makeRequest(body: SnippetRequest) {
        initApi()
        if (_error.value != null) {
            return
        }

        _isLoading.value = true
        try {
            Api.retrofitService.submitSnippet(body.id, body).enqueue(
                object: Callback<Snippet> {
                    override fun onFailure(call: Call<Snippet>, t: Throwable) {
                        _isLoading.value = false
                        _error.value = (t.message ?: "Unknown error.")
                    }
                    override fun onResponse(call: Call<Snippet>, response: Response<Snippet>) {
                        _isLoading.value = false
                        if (!response.isSuccessful()) {
                            _error.value = ("Status " + response.code() + " " +
                                (response.message() ?: "Unknown Error"))

                            return
                        }

                        _response.value = response.body()
                        _error.value = null
                    }
                }
            )
        } catch (e: Exception) {
            _error.value = e.message
        }
    }
}
