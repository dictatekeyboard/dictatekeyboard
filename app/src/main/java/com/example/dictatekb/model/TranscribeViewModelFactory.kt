package com.example.dictatekb.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import android.content.Context

import com.example.dictatekb.util.SettingsHelper

class TranscribeViewModelFactory (private val fileName: String, private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TranscribeViewModel::class.java)) {
            val helper = SettingsHelper(context)

            @Suppress("UNCHECKED_CAST")
            return TranscribeViewModel(fileName, helper) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

