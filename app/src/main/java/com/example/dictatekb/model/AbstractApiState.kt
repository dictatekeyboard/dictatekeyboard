package com.example.dictatekb.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.lang.Exception

import com.example.dictatekb.api.Api
import com.example.dictatekb.util.SettingsHelper

abstract class AbstractApiState<TReq,TRes>(private val settingsHelper: SettingsHelper) {
    protected val _isLoading: MutableLiveData<Boolean>
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    protected val _error: MutableLiveData<String>
    val error: LiveData<String>
        get() = _error

    protected val _response: MutableLiveData<TRes>
    val response: LiveData<TRes>
        get() = _response

    protected val _baseUrl: String?
        get() = settingsHelper.getBaseUrl()

    init {
        _isLoading = MutableLiveData<Boolean>()
        _error = MutableLiveData<String>()
        _response = MutableLiveData<TRes>()
    }

    protected fun initApi() {
        _error.value = null

        try {
            Api.init(_baseUrl)
        } catch (e: Exception) {
            _error.value = e.message
        }
    }

    public fun hasError(): Boolean {
        return error.value == null
    }

    public fun reset() {
        _error.value = null
        _isLoading.value = false
        _response.value = null
    }

    abstract fun makeRequest(req: TReq)
}
