package com.example.dictatekb.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import retrofit2.Callback
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.lang.IllegalStateException
import android.util.Log

import com.example.dictatekb.util.SettingsHelper
import com.example.dictatekb.util.AudioHelper
import com.example.dictatekb.error.NoRecordedDataError

class TrainViewModel(private val fileName: String, private val settingsHelper: SettingsHelper) : ViewModel() {
    val r: RecordingState
    val p: PlayingState
    val fetch: FetchSnippetState
    val upload: UploadSnippetState
    private val _recordingComplete: MutableLiveData<Boolean>
    val recordingComplete: LiveData<Boolean>
        get() = _recordingComplete

    private var audioHelper: AudioHelper

    init {
        audioHelper = AudioHelper(fileName)

        r = RecordingState(audioHelper)
        p = PlayingState(audioHelper)

        fetch = FetchSnippetState(settingsHelper)
        upload = UploadSnippetState(settingsHelper)

        _recordingComplete = MutableLiveData<Boolean>()
        _recordingComplete.value = false

        // load first snippet
        fetchNextSnippet()
    }

    fun toggleRecording() {
        if (r.isRecording.value ?: false) {
            stopRecordingAndPlay()
        } else if (p.isPlaying.value ?: false) {
            p.stopPlaying()
        } else {
            r.startRecording()
        }
    }

    fun stopRecordingAndPlay() {
        try {
            r.stopRecording()
        } catch (e: NoRecordedDataError) {
            return
        }

        p.startPlaying()
        _recordingComplete.value = true
    }

	override fun onCleared() {
        super.onCleared()
        audioHelper.clear()
    }

    fun upload() {
        val snippet = fetch.response.value
        val audio = audioHelper.getBase64()

        // ensure snippet and all it's fields are not null, for a valid request
        if (snippet?.token == null) {
            throw Error("Cannot upload audio: malformed snippet received.")
        }

        val body = SnippetRequest(
            id = snippet.id,
            status = "y",
            audio = audio,
            token = snippet.token
        )

        upload.makeRequest(body)
    }

    fun fetchNextSnippet() {
        _recordingComplete.value = false
        upload.reset()
        fetch.makeRequest(Unit)
    }

    // stop recording if in progress, and reset the state back to start
    fun cancel() {
        r.reset()
    }
}
