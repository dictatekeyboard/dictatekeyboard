package com.example.dictatekb.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.example.dictatekb.util.AudioHelper

class RecordingState(private val audioHelper: AudioHelper) {
    private val _isRecording: MutableLiveData<Boolean>
    val isRecording: LiveData<Boolean>
        get() = _isRecording

    init {
        _isRecording = MutableLiveData<Boolean>()
    }

    // begin recording
    fun startRecording() {
        audioHelper.startRecording()
        _isRecording.value = true
    }

    fun stopRecording() {
        _isRecording.value = false
        audioHelper.stopRecording()
    }

    fun reset() {
        if (_isRecording.value?: false) {
            stopRecording()
        }

        _isRecording.value = null
    }
}
