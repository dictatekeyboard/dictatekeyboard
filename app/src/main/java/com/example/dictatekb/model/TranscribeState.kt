package com.example.dictatekb.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Callback
import retrofit2.Call
import retrofit2.Response
import android.util.Log
import java.lang.Exception

import com.example.dictatekb.api.Api
import com.example.dictatekb.api.response.Transcript
import com.example.dictatekb.util.SettingsHelper
typealias TranscribeRequest =
    com.example.dictatekb.api.request.Transcribe
typealias TranscribeResponse =
    com.example.dictatekb.api.response.Transcribe

class TranscribeState(helper: SettingsHelper): AbstractApiState<String, Transcript>(helper) {
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun makeRequest(audio: String) {
        initApi()
        if (_error.value != null) {
            return
        }

        _isLoading.value = true
        val body = TranscribeRequest(audio)
        try {
            Api.retrofitService.transcribe(body).enqueue(
                object: Callback<TranscribeResponse> {
                    override fun onFailure(call: Call<TranscribeResponse>, t: Throwable) {
                        _error.value = (t.message ?: "Error making API request")
                        _isLoading.value = false
                    }
                    override fun onResponse(call: Call<TranscribeResponse>, response: Response<TranscribeResponse>) {
                        _isLoading.value = false
                        if (!response.isSuccessful()) {
                            _error.value = ("Error " + response.code())

                            return
                        }

                        val transcripts = response.body()?.transcripts ?: listOf()
                        if (transcripts.size == 0) {
                            _error.value = "No transcripts were returned."

                            return
                        }

                        _response.value = transcripts.get(0)
                        _error.value = null
                    }
                }
            )
        } catch (e: Exception) {
            _error.value = e.message
        }
    }
}
