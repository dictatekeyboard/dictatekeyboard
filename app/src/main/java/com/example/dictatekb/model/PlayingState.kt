package com.example.dictatekb.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.example.dictatekb.util.AudioHelper

class PlayingState(private val audioHelper: AudioHelper) {
    private val _isPlaying: MutableLiveData<Boolean>
    val isPlaying: LiveData<Boolean>
        get() = _isPlaying

    init {
        _isPlaying = MutableLiveData<Boolean>()
        audioHelper.setOnPlaybackFinishedListener({ _isPlaying.value = false })
    }

    fun startPlaying() {
        _isPlaying.value = true
        audioHelper.startPlaying()
    }

    fun stopPlaying() {
        audioHelper.stopPlaying()
        _isPlaying.value = false
    }
}
