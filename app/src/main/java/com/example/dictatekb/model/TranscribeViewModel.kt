package com.example.dictatekb.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import retrofit2.Callback
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import android.util.Log

import com.example.dictatekb.util.SettingsHelper
import com.example.dictatekb.util.AudioHelper
import com.example.dictatekb.error.NoRecordedDataError

class TranscribeViewModel(private val fileName: String, private val settingsHelper: SettingsHelper) : ViewModel() {
    val r: RecordingState
    val api: TranscribeState
    private var audioHelper: AudioHelper

    init {
        audioHelper = AudioHelper(fileName)
        r = RecordingState(audioHelper)
        api = TranscribeState(settingsHelper)
    }

    fun record () {
        r.startRecording()
    }

    fun stopAndTranscribe() {
        try {
            r.stopRecording()
        } catch (e: NoRecordedDataError) {
            // thrown when there is no data recorded - i.e., when stop is called straight after start.
            return
        }

        val audio = audioHelper.getBase64()
        api.makeRequest(audio)
    }

	override fun onCleared() {
        super.onCleared()
        audioHelper.clear()
    }

    fun isRecording(): Boolean {
        return r.isRecording.value ?: false
    }

    // stop recording if in progress, and reset the state back to start
    fun cancel() {
        r.reset()
        api.reset()
    }
}
