package com.example.dictatekb.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import android.os.Vibrator
import android.view.MotionEvent
import androidx.core.view.MotionEventCompat
import androidx.databinding.DataBindingUtil
import android.content.Context
import android.widget.Toast

import com.example.dictatekb.R
import com.example.dictatekb.CLICK_VIBRATION
import com.example.dictatekb.databinding.FragmentTranscribeBinding
import com.example.dictatekb.util.FILE_EXTENSION
import com.example.dictatekb.util.TouchListener
import com.example.dictatekb.model.TranscribeViewModel
import com.example.dictatekb.model.TranscribeViewModelFactory

class TranscribeFragment: Fragment() {
    // TODO clear state and refresh api url when returning to this fragment
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fileName = "${requireActivity().externalCacheDir?.absolutePath}/dictatekb" + FILE_EXTENSION
        val binding: FragmentTranscribeBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_transcribe,
            container,
            false
        )
        binding.lifecycleOwner = getViewLifecycleOwner()

        transcribeViewModelFactory = TranscribeViewModelFactory(fileName, requireActivity())
        transcribeViewModel = ViewModelProvider(this, transcribeViewModelFactory)
            .get(TranscribeViewModel::class.java)
        binding.vm = transcribeViewModel

        // add an observer to vibrate when recording starts and stops
        val vibrator = requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        transcribeViewModel.r.isRecording.observe(
            getViewLifecycleOwner(), Observer { value ->
                if (value != null) {
                    vibrator.vibrate(CLICK_VIBRATION)
                }
            }
        )

        // error toast observer
        transcribeViewModel.api.error.observe(
            getViewLifecycleOwner(), Observer {error ->
                if (error != null) {
                    Toast.makeText(
                        requireActivity(),
                        error,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )

        // add a touch listener to the rood view
        binding.recognitionResultTextView.setOnTouchListener(
            object: TouchListener() {
                override fun onDown(): Boolean {
                    transcribeViewModel.record()

                    return true
                }
                override fun onUp(): Boolean {
                    transcribeViewModel.stopAndTranscribe()

                    return true
                }
                override fun onCancel(): Boolean {
                    transcribeViewModel.cancel()

                    return true
                }
            }
        )
        // TODO override performClick

        return binding.root
    }

    override fun onPause() {
        super.onPause()

        transcribeViewModel.cancel()
    }

    private lateinit var transcribeViewModel: TranscribeViewModel
    private lateinit var transcribeViewModelFactory: TranscribeViewModelFactory
}
