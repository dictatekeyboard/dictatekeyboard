package com.example.dictatekb.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import android.os.Vibrator
import android.view.MotionEvent
import androidx.databinding.DataBindingUtil
import android.content.Context
import android.widget.Toast

import com.example.dictatekb.R
import com.example.dictatekb.CLICK_VIBRATION
import com.example.dictatekb.databinding.FragmentTrainBinding
import com.example.dictatekb.util.FILE_EXTENSION
import com.example.dictatekb.util.TouchListener
import com.example.dictatekb.model.TrainViewModel
import com.example.dictatekb.model.TrainViewModelFactory

class TrainFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fileName = "${requireActivity().externalCacheDir?.absolutePath}/dictatekb-train" + FILE_EXTENSION
        val binding: FragmentTrainBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_train,
            container,
            false
        )
        binding.lifecycleOwner = getViewLifecycleOwner()

        trainViewModelFactory = TrainViewModelFactory(fileName, requireActivity())
        trainViewModel = ViewModelProvider(this, trainViewModelFactory)
            .get(TrainViewModel::class.java)
        binding.vm = trainViewModel

        // add an observer to vibrate when recording starts and stops
        val vibrator = requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        trainViewModel.r.isRecording.observe(
            getViewLifecycleOwner(), Observer { value ->
                if (value != null) {
                    vibrator.vibrate(CLICK_VIBRATION)
                }
            }
        )

        // error toast obzerver
        trainViewModel.upload.error.observe(
            getViewLifecycleOwner(), Observer {error ->
                if (error != null) {
                    Toast.makeText(
                        requireActivity(),
                        error,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )

        trainViewModel.upload.response.observe(
            getViewLifecycleOwner(), Observer {response ->
                if (response != null) {
                    Toast.makeText(
                        requireActivity(),
                        "Snippet uploaded successfully!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        )
        binding.snippetTextView.setOnTouchListener(
            object: TouchListener() {
                override fun onDown(): Boolean {
                    trainViewModel.r.startRecording()

                    return true
                }
                override fun onUp(): Boolean {
                    trainViewModel.stopRecordingAndPlay()

                    return true
                }
                override fun onCancel(): Boolean {
                    trainViewModel.cancel()

                    return true
                }
            }
        )
        // TODO override performClick

        return binding.root
    }

    override fun onPause() {
        super.onPause()

        trainViewModel.cancel()
    }

    private lateinit var trainViewModelFactory: TrainViewModelFactory
    private lateinit var trainViewModel: TrainViewModel
}
