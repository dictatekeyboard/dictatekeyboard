package com.example.dictatekb.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import android.os.Vibrator
import android.os.VibrationEffect
import android.view.MotionEvent
import androidx.databinding.DataBindingUtil
import android.content.Context

import com.example.dictatekb.R
import com.example.dictatekb.databinding.FragmentAboutBinding
import com.example.dictatekb.model.SettingsViewModel
import com.example.dictatekb.model.SettingsViewModelFactory

class AboutFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAboutBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_about,
            container,
            false
        )
        binding.lifecycleOwner = getViewLifecycleOwner()

        settingsViewModelFactory = SettingsViewModelFactory(requireActivity())
        settingsViewModel = ViewModelProvider(this, settingsViewModelFactory)
            .get(SettingsViewModel::class.java)
        binding.viewModel = settingsViewModel

        return binding.root
    }

    private lateinit var settingsViewModelFactory: SettingsViewModelFactory
    private lateinit var settingsViewModel: SettingsViewModel
}
