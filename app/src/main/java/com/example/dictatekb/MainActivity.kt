package com.example.dictatekb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.Manifest
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import androidx.navigation.ui.NavigationUI
import androidx.databinding.DataBindingUtil
import android.view.*
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import android.os.VibrationEffect

import com.example.dictatekb.databinding.ActivityMainBinding

public val CLICK_VIBRATION = VibrationEffect.createPredefined(VibrationEffect.EFFECT_CLICK)
public const val LOG_TAG = "DictateKb"

private const val REQUEST_RECORD_AUDIO_PERMISSION = 200

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        drawerLayout = binding.drawerLayout

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

        // set up navigation
        val navController = findNavController(R.id.navHost)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        NavigationUI.setupWithNavController(binding.navView, navController)
    }

    // closes the app if permission to record is denied
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val granted = requestCode == REQUEST_RECORD_AUDIO_PERMISSION
             && grantResults[0] == PackageManager.PERMISSION_GRANTED

        if (!granted) finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.navHost)
        return NavigationUI.navigateUp(navController, drawerLayout)
    }

    private val permissions = arrayOf(Manifest.permission.RECORD_AUDIO)
    private lateinit var drawerLayout: DrawerLayout
}

