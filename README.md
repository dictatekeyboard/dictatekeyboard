# DictateKeyboard

A privacy-friendly dictation keyboard (Android front-ent).

## Compile and Install

```
# create a local.properties
# for gradle to work properly, it needs to know where the android SDK is located, we assume it's in ~/Android/Sdk
(please change accordingly)
echo "sdk.dir=${HOME}/Android/Sdk" > local.properties
# do an gradle
./gradlew
./gradlew build
./gradlew InstallDebug
```

